console.log(document);
console.log(document.querySelector('#txt-first-name'));

// result - element from html
/*
  document - whole webpage
  querySelector - selects a specific element (object)

*/

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

/*
  event: action user is doing on page

  addEventListener - function that lets the webpage listen to user events
    - takes two args
        - string: event to listen for
        - function: function to execute

*/

txtFirstName.addEventListener('keyup', (event) => {
  spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event) => {
  console.log(event);
  console.log(event.target);
  console.log(event.target.value);
});

/*
  Make another keyup event where the span event will record the last name in
  the forms.
*/

const writeName = (event) => {
  spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
};

txtFirstName.addEventListener('keyup', writeName);

txtLastName.addEventListener('keyup', writeName);
